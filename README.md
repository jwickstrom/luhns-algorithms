## Notes:

Biggest problem was throwing exceptions from the program. Even though the Junit5 test were.. 
.. giving correct exceptions, the program, seemingly, did not reach them. 
Especially in the getLuhnAlgorithm() method, where looking for zeros only(000) or input.length() < 1;






# Task: Luhn algorithm

Create a Command Line App that takes an input of numbers (any length).

It must use the Luhn Algorithm to perform a simple checksum on the last digit of the user input.

The solution must display the following:

    The full user input, with the check digit separated
    A comparison of the provided check digit compared to the expected (calculated) check digit
    A clear output of Valid or Invalid for the user input
    It must indicate if the solution is long enough to be a credit card number (16 digits)

For example, providing 4242424242424242:

Input: 424242424242424 2
Provided: 2
Expected: 2

Checksum: Valid
Digits: 16 (credit card) 

You can find a simple checking tool online here.

You must write unit test for your code:

    Write 10 total tests for you code
    Atleast 5 tests must be unique (You cannot just repeat the same tests with differnt values!)
    Use Junit5 (not 4)

Submission: Provide a link to you git repo for your solution.