package se.experis;

import java.util.Scanner;

public class LuhnProgram {

    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input?");
        String inputString = scanner.nextLine();

        //checkExceptionInString(inputString);
        int luhnOutput = getLuhnAlgorithm(inputString);

        //Finding last character in above String, making it into an Integer.
        char lastNumb = inputString.charAt(inputString.length()-1);
        int lastNumbToInt = Character.getNumericValue(lastNumb);

        //The method inserts a space to inputted string
        System.out.print("\nInput: " + getSplittedNumbers(inputString) + "\n");

        //Printing the numbers and its design.
        System.out.println("Provided: " + lastNumb);

        //int luhnOutput = getLuhnAlgorithm(inputString);
        System.out.println("Expected: " + luhnOutput + "\n");

        System.out.print("Checksum: ");

        if(validityTesting(luhnOutput, lastNumbToInt)){
            System.out.println("Valid");
        } else{
            System.out.println("Invalid");
        }

        System.out.println(printDigitsAndCreditCard(inputString));


    }

    //Checking whether inputSting is 16 digits. If so, (Credit card).
    public static String printDigitsAndCreditCard(String inputString) {
        if(inputString.length() == 16){
            return "Digits: " + inputString.length() + " (Credit card).";
        } else {
            return "Digits: " + inputString.length() + " (NOT Credit card).";
        }
    }

    //Testing last number of inputString to outcome of Luhn's
    public static boolean validityTesting(int luhnOutput, int lastNumbToInt) {
        return luhnOutput == lastNumbToInt;
    }

    //Inserting a space in between the last two characters in inputString
    public static String getSplittedNumbers(String shortNumb) {

        StringBuilder stringBuilder = new StringBuilder(shortNumb);
        shortNumb = stringBuilder.insert(shortNumb.length()-1, " ").toString();

        return shortNumb;
    }

    public static int getLuhnAlgorithm(String shortNumb) throws Exception {
        //First checking if inputString is only zeros or length<1.
        //Wanted to have in a separate method but didn't manage. Cuz the order of the code got screwed somehow.
        boolean correct = false;
        for(char c:shortNumb.toCharArray())
            if(c!='0'){
                correct = true;
                break;
            }
        if (!correct)
            { //These Exceptions doesn't seem to go trough, in Test they do though..
                throw new Exception("Only zeros in String");
            }
        if(shortNumb.length() <=1) {
            throw new Exception("To short of a number!");
        }

        //Luhns math below
        //Every value, according to Luhns formula, is added to the sum.
        int sum = 0;

        boolean tickerForDouble = false;

        //length -2 because the final value in inputString is only provided ad checker!
        for (int i = shortNumb.length() - 2; i >= 0; i--)
        {
            int n = Integer.parseInt(shortNumb.substring(i, i+1));

            if(!tickerForDouble){
                n = n*2;
                if (n > 9)
                {
                    n = (n % 10) + 1;
                }

            }
            tickerForDouble = !tickerForDouble;
            sum += n;
        }
        sum = sum*9 %10; //multiply by 9 for some reason, then return rightmost number.
        return sum;
        }
    }




