import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import se.experis.LuhnProgram;

import static org.junit.jupiter.api.Assertions.*;
import static se.experis.LuhnProgram.*;

class LuhnProgramTest {

    @Test
    public void testMain() {

        // Does it inserts a space right before the last number?
        assertEquals(getSplittedNumbers("424242424242424"), "42424242424242 4");
        assertEquals(getSplittedNumbers("12"), "1 2");

    }

    @Test //Is the Luhn-math correct?
    public void LuhnTest() throws Exception {
        assertEquals(getLuhnAlgorithm("799273987112"), 2);
        assertEquals(getLuhnAlgorithm("424242424242424"), 4);

    }
    //Both of these tests function fine, but when trying in program's main, it still..
    //..won't return the Exception..
    @Test
    public void checkException() throws Exception {
       String k = "1";
        Assertions.assertThrows(Exception.class, () -> {
            getLuhnAlgorithm(k);
        });

        String j = "0000000";
        Assertions.assertThrows(Exception.class, () -> {
            getLuhnAlgorithm(j);
        });

    }
    @Test //Check validity, (equality)
    public void validChecker()  {
        //Should return true if they are equal.
        assertTrue(LuhnProgram.validityTesting(4, 4));

    }

    @Test //Is the printing output correct?
    public void getCreditCardLength()  {
        String test = printDigitsAndCreditCard("234");

        assertEquals("Digits: 3 (NOT Credit card).", test);

    }
}